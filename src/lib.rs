mod glyphs;

use glyphs::{fivefont::FiveFont, tinyfont::TinyFont, Font, Glyph};
use hidapi::{HidApi, HidDevice};
use std::{collections::VecDeque, thread, time::Duration};
use thiserror::Error;

const USAGE_PAGE: u16 = 0xFF60;
const USAGE_ID: u16 = 0x61;
const EPSIZE: usize = 32;

pub type Result<T> = ::std::result::Result<T, KeyboardError>;

pub struct Buffer {
    data: VecDeque<Vec<(u8, u8, u8)>>,
}

impl Buffer {
    fn from_text<S: AsRef<str>, F: Font>(text: S, font: &F, height: u8) -> Self {
        let glyphs: Vec<Glyph> = text
            .as_ref()
            .chars()
            .filter_map(|c| font.glyph(c))
            .collect();
        let mut data = VecDeque::new();
        for glyph in glyphs {
            let mut tmp: VecDeque<_> = glyph
                .data
                .into_iter()
                .map(|v| {
                    let mut colors: Vec<(u8, u8, u8)> = v
                        .into_iter()
                        .take(height as usize)
                        .map(|x| if x { (255, 255, 255) } else { (0, 0, 0) })
                        .collect();
                    while colors.len() < height as usize {
                        colors.push((0, 0, 0));
                    }
                    colors
                })
                .collect();
            while tmp
                .front()
                .map(|x| x.iter().all(|x| x == &(0, 0, 0)))
                .unwrap_or(false)
            {
                tmp.pop_front();
            }
            while tmp
                .back()
                .map(|x| x.iter().all(|x| x == &(0, 0, 0)))
                .unwrap_or(false)
            {
                tmp.pop_back();
            }
            data.extend(tmp);
            data.extend(vec![vec![(0, 0, 0); height as usize]; 1 + glyph.space]);
        }
        Buffer { data }
    }
}

#[derive(Debug, Error)]
pub enum KeyboardError {
    #[error("device not found")]
    DeviceNotFound,
    #[error("could not open device: {0}")]
    Open(#[from] hidapi::HidError),
    #[error("could not write to device: {0}")]
    Write(#[from] std::io::Error),
    #[error("unexpected response: {0:?}")]
    UnexpectedResponse(Vec<u8>),
    #[error("multithreading error")]
    ThreadingError,
}

pub struct Keyboard {
    device: HidDevice,
}

impl Keyboard {
    pub fn open() -> Result<Self> {
        // 9600 baud, 1 stop bit, no parity, 8 data bits
        let api = HidApi::new()?;

        let device_info = api
            .device_list()
            .find(|x| x.usage() == USAGE_ID && x.usage_page() == USAGE_PAGE)
            .ok_or(KeyboardError::DeviceNotFound)?;
        let device = device_info.open_device(&api)?;

        let keyboard = Keyboard { device };
        Ok(keyboard)
    }

    pub fn read(&self) -> Result<Vec<u8>> {
        let mut buf = [0; EPSIZE];
        self.device.read_timeout(&mut buf, 1000)?;
        let len = buf[0];
        Ok(buf.iter().skip(1).take(len as usize).copied().collect())
    }

    pub fn send(&mut self, command: Command) -> Result<()> {
        self.send_multiple(vec![command])
    }

    pub fn send_multiple(&mut self, commands: Vec<Command>) -> Result<()> {
        for package in commands.into_iter().flat_map(|x| x.packages()) {
            let mut buf = [0; EPSIZE + 1];
            for (i, x) in package.into_iter().enumerate() {
                buf[i + 1] = x;
            }
            self.device.write(&buf)?;
        }
        Ok(())
    }

    pub fn canvas_size(&mut self) -> Result<(u8, u8)> {
        self.send(Command::Size)?;
        let resp = self.read()?;
        if resp.len() != 2 {
            Err(KeyboardError::UnexpectedResponse(resp))
        } else {
            Ok((resp[0], resp[1]))
        }
    }

    pub fn text<S: AsRef<str>>(&mut self, text: S, wrap: bool, delay: Duration) -> Result<()> {
        let (h, w) = self.canvas_size()?;
        let mut buffer = if h == 4 {
            Buffer::from_text(text, &TinyFont(), h)
        } else {
            Buffer::from_text(text, &FiveFont(), h)
        };
        if wrap {
            let mut data: VecDeque<Vec<_>> = buffer.data.clone();
            data.push_back(vec![(0, 0, 0); w as usize]);
            data.push_back(vec![(0, 0, 0); w as usize]);
            while data.len() < w as usize {
                data.extend(buffer.data.clone());
                data.push_back(vec![(0, 0, 0); w as usize]);
                data.push_back(vec![(0, 0, 0); w as usize]);
            }
            for _ in 0..(buffer.data.len() as u8 + w) {
                let data_slice: Vec<_> = data.iter().take(w as usize).cloned().collect();
                self.send(Command::Image(data_slice))?;
                thread::sleep(delay);
                data.rotate_left(1);
            }
        } else {
            let mut data: VecDeque<_> = vec![vec![(0, 0, 0); h as usize]; w as usize].into();
            for _ in 0..(buffer.data.len() as u8 + w) {
                let next = buffer
                    .data
                    .pop_front()
                    .unwrap_or_else(|| vec![(0, 0, 0); h as usize]);
                data.pop_front();
                data.push_back(next);
                self.send(Command::Image(data.clone().into()))?;
                thread::sleep(delay);
            }
        }
        Ok(())
    }
}

pub enum Command {
    RGBMode(u8),
    Color(u8, u8, u8),
    Pixel(u8, u8, u8, u8, u8),
    LED(u8, u8, u8, u8),
    Image(Vec<Vec<(u8, u8, u8)>>),
    RGBSave,
    RGBRestore,
    Progress(u8, u8, bool, f64, u8, u8, u8),
    Size,
    Layer(u8),
}

impl Command {
    fn packages(self) -> Vec<Vec<u8>> {
        match self {
            Command::RGBMode(mode) => vec![vec![1, mode]],
            Command::Color(r, g, b) => vec![vec![2, r, g, b]],
            Command::Pixel(x, y, r, g, b) => vec![vec![3, x, y, r, g, b]],
            Command::Image(data) => {
                // TODO more than one pixel per package
                data.into_iter()
                    .enumerate()
                    .flat_map(|(x, v)| v.into_iter().enumerate().map(move |(y, c)| (x, y, c)))
                    .flat_map(|(x, y, (r, g, b))| {
                        Command::Pixel(x as u8, y as u8, r, g, b).packages()
                    })
                    .collect()
            }
            Command::RGBSave => vec![vec![4]],
            Command::RGBRestore => vec![vec![5]],
            Command::Progress(ind, w, enabled, progress, r, g, b) => {
                vec![vec![
                    6,
                    ind,
                    u8::from(enabled),
                    (progress * (w as f64)).round() as u8,
                    r,
                    g,
                    b,
                ]]
            }
            Command::Size => vec![vec![7], vec![0, 0]],
            Command::LED(i, r, g, b) => vec![vec![8, i, r, g, b]],
            Command::Layer(l) => vec![vec![9, l]],
        }
    }
}
