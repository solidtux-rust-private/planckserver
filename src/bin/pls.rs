use planck::{Command, Keyboard};
use std::{thread, time::Duration};
use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about, author)]
struct Opt {
    #[structopt(subcommand)]
    mode: Mode,
}

#[derive(StructOpt)]
#[allow(clippy::enum_variant_names)]
enum Mode {
    /// Set RGB animation mode
    RGBMode {
        mode: u8,
    },
    /// Set full keyboard to RGB color
    Color {
        r: u8,
        g: u8,
        b: u8,
    },
    /// Set single LED to RGB color
    Led {
        i: u8,
        r: u8,
        g: u8,
        b: u8,
    },
    /// Set single Pixel to RGB color
    Pixel {
        x: u8,
        y: u8,
        r: u8,
        g: u8,
        b: u8,
    },
    /// Render text on keyboard
    Text {
        #[structopt(short, long)]
        wrap: bool,
        text: String,
    },
    /// Set current default layer
    Layer {
        layer: u8,
    },
    /// Save current RGB mode
    RGBSave,
    /// Restore current RGB mode
    RGBRestore,
    /// Alarm blinking
    Alarm,
    /// Set progress bar
    Progress {
        ind: u8,
        progress: Option<f64>,
    },
    LEDTest {
        start: u8,
        stop: u8,
    },
    PixelTest,
    Size,
}

fn main_res() -> anyhow::Result<()> {
    let opt = Opt::from_args();
    let mut keyboard = Keyboard::open()?;

    match opt.mode {
        Mode::RGBMode { mode } => {
            keyboard.send(Command::RGBMode(mode))?;
        }
        Mode::Color { r, g, b } => {
            keyboard.send(Command::Color(r, g, b))?;
        }
        Mode::Pixel { x, y, r, g, b } => {
            keyboard.send(Command::Pixel(x, y, r, g, b))?;
        }
        Mode::Led { i, r, g, b } => {
            keyboard.send(Command::LED(i, r, g, b))?;
        }
        Mode::Text { wrap, text } => {
            keyboard.send(Command::RGBSave)?;
            keyboard.send(Command::Color(0, 0, 0))?;
            keyboard.text(text, wrap, Duration::from_millis(100))?;
            keyboard.send(Command::Color(0, 0, 0))?;
            keyboard.send(Command::RGBRestore)?;
        }
        Mode::Layer { layer } => {
            keyboard.send(Command::Layer(layer))?;
        }
        Mode::Alarm => {
            keyboard.send(Command::RGBSave)?;
            for _ in 0..5 {
                keyboard.send(Command::Color(255, 0, 0))?;
                thread::sleep(Duration::from_millis(200));
                keyboard.send(Command::Color(0, 0, 0))?;
                thread::sleep(Duration::from_millis(100));
            }
            keyboard.send(Command::RGBRestore)?;
        }
        Mode::RGBSave => {
            keyboard.send(Command::RGBSave)?;
        }
        Mode::RGBRestore => {
            keyboard.send(Command::RGBRestore)?;
        }
        Mode::Progress { ind, progress } => {
            let (_, w) = keyboard.canvas_size()?;
            match progress {
                Some(p) => {
                    keyboard.send(Command::Progress(ind, w, true, p, 255, 255, 255))?;
                }
                None => {
                    keyboard.send(Command::Progress(ind, w, false, 0., 255, 255, 255))?;
                }
            }
        }
        Mode::Size => {
            let (h, w) = keyboard.canvas_size()?;
            println!("{w}x{h}");
        }
        Mode::LEDTest { start, stop } => {
            keyboard.send(Command::RGBSave)?;
            keyboard.send(Command::Color(0, 0, 0))?;
            for ind in start..=stop {
                println!("{ind:>3}");
                keyboard.send(Command::LED(ind, 255, 0, 0))?;
                thread::sleep(Duration::from_millis(200));
                keyboard.send(Command::LED(ind, 0, 0, 0))?;
            }
            keyboard.send(Command::RGBRestore)?;
        }
        Mode::PixelTest => {
            let (h, w) = keyboard.canvas_size()?;
            keyboard.send(Command::RGBSave)?;
            keyboard.send(Command::Color(0, 0, 0))?;
            for x in 0..w {
                keyboard
                    .send_multiple((0..h).map(|y| Command::Pixel(x, y, 255, 0, 0)).collect())?;
                thread::sleep(Duration::from_millis(200));
                keyboard.send(Command::Color(0, 0, 0))?;
            }
            for y in 0..h {
                keyboard
                    .send_multiple((0..w).map(|x| Command::Pixel(x, y, 255, 0, 0)).collect())?;
                thread::sleep(Duration::from_millis(200));
                keyboard.send(Command::Color(0, 0, 0))?;
            }
            keyboard.send(Command::RGBRestore)?;
        }
    }

    Ok(())
}

fn main() {
    if let Err(e) = main_res() {
        eprintln!("{}", e);
        eprintln!("{}", e.backtrace());
    }
}
